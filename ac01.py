from flask import Flask

app = Flask(_name_)

@app.route('/media/<nota1>/<nota2>')
def calcular_media(nota1, nota2):
    nota1 = float(nota1)
    nota2 = float(nota2)
    media = (nota1 + nota2) / 2
    return f'A média das notas {nota1} e {nota2} é: {media}'

if _name_ == '_main_':
    app.run()